﻿using Microsoft.EntityFrameworkCore;
using EntityFramework.Models;

namespace EntityFramework.Data
{
    /* Context Class: Middleman between program and the database */
    public class CountryContext: DbContext
    {
        /*Initialize DBContext Constructor*/
        public CountryContext(DbContextOptions<CountryContext> options): base(options)
        {

        }

        /*Gateway to the actual tables*/
        public DbSet<Country> Countries { get; set; }
        public DbSet<Continent> Continents { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Country>().ToTable("Country");
            modelBuilder.Entity<Continent>().ToTable("Continent");
        }
    }
}
