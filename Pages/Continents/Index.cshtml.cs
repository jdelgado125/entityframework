using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using EntityFramework.Models;
using EntityFramework.Data;

namespace EntityFramework.Pages.Continents
{
    public class IndexModel : PageModel
    {
        private readonly CountryContext _context;

        public IndexModel(CountryContext context)
        {
            _context = context;
        }

        public List<Continent> Continents { get; set; }

        public async Task OnGetAsync()
        {
            Continents = await _context.Continents.ToListAsync();
        }

        public async Task<IActionResult> OnPostAsync(string id)
        {
            if(id == null)
            {
                return NotFound();
            }

            //Return the row with the specified id
            Continent Continent = await _context.Continents.FindAsync(id);
            if(Continent != null)
            {
                //Remove the row if it exists
                _context.Continents.Remove(Continent);
            }

            //Persist the changes to the DB
            await _context.SaveChangesAsync();

            //Refresh the page
            return RedirectToPage("./Index");
        }
    }
}
