﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using EntityFramework.Data;
using EntityFramework.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace EntityFramework.Pages.Countries
{
    public class IndexModel : PageModel
    {
        private readonly CountryContext _context;

        public IndexModel(CountryContext context)
        {
            _context = context;
        }

        public List<Country> Countries { get;set; }
        [BindProperty(SupportsGet = true)]
        public string SearchString { get; set; }
        [BindProperty(SupportsGet = true)]
        public string SortField { get; set; } = "Name";
        [BindProperty(SupportsGet = true)]
        public string SelectedContinent { get; set; }
        public SelectList Continents { get; set; }

        public async Task OnGetAsync()
        {
            var countries = from c in _context.Countries
                            select c;

            if(!string.IsNullOrEmpty(SearchString))
            {
                countries = countries.Where(c => c.Name.Contains(SearchString));
            }

            switch (SortField)
            {
                case "ID":
                    countries = countries.OrderBy(c => c.ID);
                    break;
                case "Name":
                    countries = countries.OrderBy(c => c.Name);
                    break;
                case "ContinentID":
                    countries = countries.OrderBy(c => c.ContinentID);
                    break;
            }



            var continents = from cn in _context.Continents
                             orderby cn.ID
                             select cn.ID;

            Continents = new SelectList(await continents.ToListAsync());

            Countries = await countries.ToListAsync();
        }

        public async Task<IActionResult> OnPostAsync(string id)
        {
            if(id == null)
            {
                return NotFound();
            }

            //Return one row of Country by specified id
            Country Country = await _context.Countries.FirstOrDefaultAsync(c => c.ID == id);

            if(Country != null)
            {
                //Remove from DB if exists
                _context.Countries.Remove(Country);
            }

            //Persist the changes
            await _context.SaveChangesAsync();

            //Reload the page
            return RedirectToPage("./Index");
        }
    }
}
