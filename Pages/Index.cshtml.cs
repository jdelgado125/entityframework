﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using EntityFramework.Data;
using EntityFramework.Models;
using Microsoft.Extensions.Logging;

namespace EntityFramework.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly CountryContext _context;

        public IndexModel(ILogger<IndexModel> logger, CountryContext context)
        {
            _logger = logger;
            _context = context;
        }

        public List<Country> Countries { get; set; }
        public List<Continent> Continents { get; set; }

        public async Task OnGetAsync()
        {
            Continents = await _context.Continents.ToListAsync();
            Countries = await _context.Countries.ToListAsync();
        }
    }
}
